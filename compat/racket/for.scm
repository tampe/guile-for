;-------------------------------------------------------------------------------
(define-module (compat racket for)
  #:use-module (syntax parse)
  #:use-module (srfi srfi-11)
  #:use-module (ice-9 pretty-print)
  #:export (for/fold for/list for/and for/or for/sum for/product for for/first
                     for/last for/vector for/hash for/hasheq for/hasheqv
                     for/lists
            for*/fold for*/list for*/and for*/or for*/sum for*/product for* 
                     for*/first for*/last for*/lists
                     for*/vector for*/hash for*/hasheq for*/hasheqv))


(define-syntax true     (lambda (x) #'#t))
(define-syntax idd      (syntax-rules () ((_ x) x)))
(define-syntax add-one  (syntax-rules () ((_ x) (+ x 1))))


(begin
(define-syntax-class rng
  (pattern ((~datum in-range) end)
	   #:with a #'0
	   #:with b #'end
	   #:with c #'1)
  (pattern ((~datum in-range) start end)
	   #:with a #'start
	   #:with b #'end
	   #:with c #'1)
  (pattern ((~datum in-range) start step end)
	   #:with a #'start
	   #:with b #'end
	   #:with c #'step))

(define-splicing-syntax-class x-cond-when
  (pattern (~seq #:when guard)
           #:with condition #'guard)
  (pattern (~seq #:unless guard)
           #:with condition #'(not guard)))

(define-splicing-syntax-class x-cond-break
  (pattern (~seq #:break guard)
           #:with continue #'(not guard)))

(define-splicing-syntax-class x-cond-final
  (pattern (~seq #:final guard)
           #:with final? #'guard))

(define (stx-nonempty? x)
  (syntax-case x ()
    (() #f)
    (_  #t)))

(define-splicing-syntax-class fail
  (pattern (~seq #:fail f)
           #:with fail #'f)
  (pattern (~seq)
           #:with fail #'#f))

(define-splicing-syntax-class x-cond
  (pattern (~and (~seq x ...)
                 (~seq (~or xs:x-cond-when
                            ys:x-cond-break
                            zs:x-cond-final) ...))
           #:fail-when (not (stx-nonempty? #'(xs ... ys ... zs ...)))
           "could not find ane cross directives"
           
           #:with (condition ...) #'(xs.condition ...)
           #:with (continue  ...) #'(ys.continue  ...)
           #:with (final?    ...) #'(zs.final?    ...)
           #:attr isFinal?        (stx-nonempty? #'(final? ...))))

(define-splicing-syntax-class x-cond/any
  (pattern (~and (~seq x ...)
                 (~seq (~or xs:x-cond-when
                            ys:x-cond-break
                            zs:x-cond-final) ...))
           #:with (condition ...) #'(xs.condition ...)
           #:with (continue  ...) #'(ys.continue  ...)
           #:with (final?    ...) #'(zs.final?    ...)
           #:attr isFinal?        (stx-nonempty? #'(final? ...))))

(define-syntax-class gen
  (pattern ((k:id v:id) ((~datum in-hash) ha:expr))
	   #:with (letv ...)  #'()
	   #:with (lets ...)  #`()
	   #:with (helps ...) #'()
	   #:with (helpv ...) #'()
	   #:with (init ...)  #'((hash->list ha))
	   #:with (li   ...)  (generate-temporaries #'(k))
	   #:with (pair? ...) #'((pair? li) ...)
	   #:with (car   ...) #'((k (caar li ...)) (v (cdar li ...)))
	   #:with (cdr   ...) #'((cdr li) ...))
           

  (pattern (i:id range:rng)
	   #:with (letv  ...) #'()
	   #:with (lets  ...) #`()
	   #:with (helps ...) #'()
	   #:with (helpv ...) #'()
	   #:with (init  ...) #'(range.a)
	   #:with (li    ...) #'(i)
	   #:with (pair? ...) #'((<= i range.b))
	   #:with (car   ...) #'()
	   #:with (cdr   ...) #'((+ i range.c)))

  (pattern (i:id ((~datum in-vector) vec:expr))
	   #:with (letv ...)  #'(vec)	   
	   #:with (lets ...)  #`(#,(datum->syntax #'vec (gensym "vec")))
	   #:with (helps ...) #`(#,(datum->syntax #'vec (gensym "n")))
	   #:with (helpv ...) #'((vector-length lets ...))
	   #:with (init ...)  #'(0)
	   #:with (li   ...)  #'(i)
	   #:with (pair? ...) #'((< li helps) ...)
	   #:with (car   ...) #'((i (vector-ref lets li)) ...)
	   #:with (cdr   ...) #'((+ li 1) ...))

  (pattern ( x:id (~or ((~datum in-list) l:expr)
                       (~and ((~datum quote  ) e ...)
                             l)))
                  
	   #:with (lets ...)  #'()
	   #:with (letv ...)  #'()
	   #:with (helps ...) #'()
	   #:with (helpv ...) #'()
           #:with (init ...)  #'(l)
	   #:with (li   ...)  (generate-temporaries #'(x))
           #:with (pair? ...) #'((pair? li) ...)
           #:with (car   ...) #'((x (car li)) ...)
           #:with (cdr   ...) #'((cdr li) ...))

  (pattern (i:id (~datum in-naturals))
	   #:with (lets ...)   #'()
	   #:with (letv ...)   #'()
	   #:with (helps ...)  #'()
	   #:with (helpv ...)  #'()
           #:with (init ...)   #'(0)
	   #:with (li    ...)  #'(i)
           #:with (pair? ...)  #'(#t)
           #:with (car   ...)  #'()
           #:with (cdr   ...)  #'((+ li ... 1)))

  (pattern (x:id  (~and l:expr
                        (~not ((~or (~datum in-vector)
                                    (~datum in-hash)
                                    (~datum in-list)
                                    (~datum in-range))
                               _ ...))))

	   #:with (lets ...)  #'()
	   #:with (letv ...)  #'()
	   #:with (helps ...) #'()
	   #:with (helpv ...) #'()
           #:with (init ...)  #'(l)
	   #:with (li  ...)   (generate-temporaries #'(x))
           #:with (pair? ...) #'((pair? li) ...)
           #:with (car ...)   #'((x (car li)) ...)
           #:with (cdr ...)   #'((cdr li)   ...))))


(define-syntax for/fold
  (lambda (stx)
    (syntax-parse stx
      ((_ ((foldv:id foldi:expr) ...) (rec:gen ...) code ...)
       #'(let* ((rec.lets  rec.letv)  ... ...
		(rec.helps rec.helpv) ... ...)
	   (let loop ((foldv foldi) ... (rec.li rec.init) ... ...)
	     (if (and rec.pair? ... ...)
                 (let (rec.car ... ...)
                   (let-values (((foldv ...) (begin code ...)))
                     (loop foldv ... rec.cdr ... ...)))
                 (values foldv ...))))))))

(define (pp x) 
  (pretty-print
   (let loop ((y x))
     (syntax-case y ()
       ((a . b) (cons (loop #'a) (loop #'b)))
       (x       (syntax->datum #'x)))))
  x)

(define-syntax for/first
  (lambda (stx)
    (syntax-parse stx
      ((_ (f:fail rec:gen ... cd1:x-cond/any) cd2:x-cond/any code ...)
       #'(let* ((rec.lets rec.letv) ... ...
		(rec.helps rec.helpv) ... ...)
	   (let ((rec.li rec.init) ... ...)
             (if (and rec.pair? ... ...)
                 (let (rec.car ... ...)
                   (if (and cd1.continue ... cd2.continue ...
                            cd1.condition ... cd2.condition ...)
                       (begin code ...)
                       f.fail))
                 f.fail))))
      
      ((_ (f:fail rec:gen ... cd:x-cond l ...) code ...)
       #'(let* ((rec.lets rec.letv) ... ...
		(rec.helps rec.helpv) ... ...)
	   (let ((rec.li rec.init) ... ...)
             (if (and rec.pair? ... ...)
                 (let (rec.car ... ...)
                   (if (and cd.continue ... cd.condition ...)
                       (for/first (#:fail f.fail l ... ) code ...)
                       f.fail))
                 f.fail)))))))

(define-syntax for
  (lambda (stx)
    (syntax-parse stx
      ((_ (i:fail rec:gen ... cd1:x-cond/any) cd2:x-cond/any code ...)
       #'(let* ((rec.lets rec.letv) ... ...
		(rec.helps rec.helpv) ... ...)
	   (let loop ((rec.li rec.init) ... ...)
             (if (and rec.pair? ... ...)
                 (let (rec.car ... ...)
                   (when (and cd1.continue ... cd2.continue ...)
                     (if (and cd1.condition ... cd2.condition ...)
                         (if (or cd1.final? ... cd2.final? ...)
                             (begin 
                               code ...
                               (if #f #f))
                             (begin
                               code ...
                               (loop rec.cdr ... ...)))
                         (loop rec.cdr ... ...))))
                 i.fail))))
      
      ((_ (i:fail rec:gen ... cd:x-cond l ...) code ...)
       #'(let* ((rec.lets rec.letv) ... ...
		(rec.helps rec.helpv) ... ...)
	   (let loop ((rec.li rec.init) ... ...)             
             (if (and rec.pair? ... ...)
                 (let (rec.car ... ...)
                   (if (and cd.continue ...)
                       (if (and cd.condition ...)
                           (if (or cd.final? ...)
                               (for/first (#:fail (if #f #f) l ... ) code ...)
                               (for (#:fail (loop rec.cdr ... ...)
                                            l ...) code ...))
                           (loop rec.cdr ... ...))
                       (if #f #f)))
                 i.fail)))))))
           


(define-syntax for/fold
  (lambda (x)
    (syntax-parse x
      ((_ ((acc:id init) ...) x cd:x-cond/any body ...)
       (with-syntax (((arg ...) (generate-temporaries #'(acc ...))))
         #'(let ((acc init) ...)
             (for x 
                  cd.x ...
                  (call-with-values (lambda () body ...)
                    (lambda (arg ...)
                      (set! acc arg) ...)))
             (values acc ...)))))))

(define-syntax for/lists
  (lambda (x)
    (syntax-parse x
      ((_ (acc:id ...) x cd:x-cond/any body ...)
       (with-syntax (((arg ...) (generate-temporaries #'(acc ...))))
         #'(let ((acc '()) ...)
             (for x 
                  cd.x ...
                  (call-with-values (lambda () body ...)
                    (lambda (arg ...)
                      (set! acc (cons arg acc)) ...)))
             (values (reverse acc) ...)))))))


(define-syntax-rule (for/op nm init op fini)
  (define-syntax nm
    (lambda (x)
      (syntax-parse x
        ((_ l cd:x-cond/any . code)
         #'(let ((res init))
             (for l 
                  cd.x (... ...)
                  (set! res (op (begin . code) res)))
             (fini res)))))))


(define-syntax-rule (ident x) x)
(define-syntax-rule (take-1 x y) x)
(define (mk-vec x) (list->vector (reverse x)))

(for/op for/list    '()  cons   reverse)
(for/op for/vector   '() cons   mk-vec)
(for/op for/sum      0   +      ident)
(for/op for/product  1   *      ident)
(for/op for/and      #t  and    ident)
(for/op for/last     #f  take-1 ident)


(define-syntax for/or
  (lambda (x)
    (syntax-parse x
      ((_ l cd:x-cond/any . code)
       #'(let ((res #f))
           (for l 
                cd.x ...
                #:break (let ((r (begin . code)))
                          (when r (set! res r))
                          r)
                #f)
           res)))))

(define-syntax-rule (for/h nm hset!)
  (define-syntax nm
    (lambda (x)
      (syntax-parse x
        ((_ l cd:x-cond/any . code)
         #'(let ((res (make-hash-table)))
             (for l 
                  cd.x (... ...)
                  (call-with-values (lambda () . code)
                    (lambda (k v)
                      (hset! res k v)))
                  #f)
             res))))))

(for/h for/hash hash-set!)
(for/h for/hasheq hashq-set!)
(for/h for/hasheqv hashv-set!)

;;* variants
(define (process-l x)
  (define (f x)
     (let loop ((r '()) (x (reverse x)))
       (if (pair? x)
           (loop (append (list (car x) #:when #t) r)
                 (cdr x))
           r)))

  (syntax-parse x
    ((rec:gen ...)
     (f #'(rec ...)))
    ((rec:gen ... cd:x-cond . l)
     #`(#,@(f #'(rec ...)) cd.x ... #,@(process-l #'l)))
    (() #'())))


(define-syntax-rule (mk* for* for)
  (define-syntax for*
    (lambda (x)
      (syntax-case x ()
        ((_ l . code)
         #`(for #,(process-l #'l) . code))))))

(define-syntax-rule (mk-fold* for* for)
  (define-syntax for*
    (lambda (x)
      (syntax-case x ()
        ((_ a l . code)
         #`(for a #,(process-l #'l) . code))))))

(mk*      for*         for)
(mk*      for*/list    for/list)
(mk*      for*/and     for/and)
(mk*      for*/or      for/or)
(mk*      for*/first   for/first)
(mk*      for*/last    for/last)
(mk*      for*/sum     for/sum)
(mk*      for*/product for/product)
(mk*      for*/vector  for/vector)
(mk*      for*/hash    for/hash)
(mk*      for*/hasheq  for/hasheq)
(mk*      for*/hasheqv for/hasheqv)
(mk-fold* for*/fold    for/fold)
(mk-fold* for*/lists   for/lists)
